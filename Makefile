CC=gcc
CFLAGS=-g -Wall -Wextra -std=c99
EXEC=bin/abrFibo

SRC= $(wildcard src/*.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC)

## Binary files

$(EXEC): $(OBJ)
	@$(CC) $^ -o $@
	
## Object files

src/%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(LDFLAGS)

## Actions

clean:
	@rm -rf ./$(OBJ)
	@rm -rf ./*~

mrproper: clean
	@rm -rf $(EXEC)
