#include "../include/abr.h"
#include <stdio.h>
#include<stdlib.h>

/**
 * Libère l'arbre de la mémoire
 * @param  a Arbre à libérer
 * @return   1 Si mémoire libérée, 0 si rien à libérer
 */
int liberer(t_arbre * a){
	int err = 0, val = val_racine(* a, &val);
	static int root = 0; /* Détermine si racine ou non */

	if(!arbre_vide(* a)){

		if(est_racine(* a) && root)
			root = 0;

		/* Remonte à la racine si pas encore à la racine */
		if(!root){

			while(!est_racine(* a))
				* a = pere(* a);

			root++;
		}

		/* Parcours de l'arbre postfixe */
		if(sag(* a))
			liberer(&(* a)->sag);

		if(sad(* a))
			liberer(&(* a)->sad);

		/* Libère de la mémoire la racine actuelle */
			
		/* Met à jour le nombre de références sur la racine si non nul */
		if((* a)->nbRef)
			(* a)->nbRef -= 1;
		else
			free(* a);
		
		* a = NULL; 

		err = 1;
	}



	return err;
}

/**
 * Libère le sous-arbre
 * @param  a Sous-arbre à libérer
 * @return   1 Si mémoire libérée, 0 si rien à libérer
 */
int libererSa(t_arbre * a){
	int err = 0;

	if(!arbre_vide(* a)){

		/* Parcours de l'arbre postfixe */
		if(sag(* a))
			libererSa(&(* a)->sag);

		if(sad(* a))
			libererSa(&(* a)->sad);

		/* Libère de la mémoire la racine actuelle */
			
		/* Met à jour le nombre de références sur la racine si non nul */
		if((* a)->nbRef)
			(* a)->nbRef -= 1;
		else
			free(* a);
		
		* a = NULL; 

		err = 1;
	}

	return err;
}

/**
 * Crée un arbre de Fibonacci
 * @param a Arbre résultant
 */
void creer_abrFibo(t_arbre * a){
	int n, nb1, nb2, i;
	liberer(a); /* Libération de l'arbre actuel */

	printf("Saisissez l'ordre n de l'arbre: ");
	scanf("%i", &n);

	if(n < 3){
		fprintf(stderr, "Ordre trop faible, entrez un ordre de 3 minimum.\n");
		return ;
	}

	/* Crée la base de l'arbre */
	* a = creer_arbre(2, NULL, NULL, NULL);
	ajout_gauche(* a, 1);
	ajout_droit(* a, 1);

	for(i = 3; i < n; i++){
		
		if(sag(* a) && sad(* a)){

			val_racine(* a, &nb1);
			val_racine(sad(* a), &nb2);

			/* Nouvelle référence sur le sous-arbre droit */
			(* a)->sad->nbRef += 1;

			/* Augmente l'ordre de 1 */
			* a = creer_arbre(nb1 + nb2, sad(* a), * a, NULL);

			/* Met à jour le père du sad (pas de sag car copie par référence) */
			(* a)->sad->pere = * a;
		}
	}

}

/**
 * Augmente l'ordre de l'arbre de 1
 * @param a Arbre de Fibonacci à modifier
 */
void augmenterFibo(t_arbre * a){
	int nb1, nb2;

	if(!arbre_vide(* a)){
		val_racine(* a, &nb1);
		val_racine(sad(* a), &nb2);
		
		/* Nouvelle référence sur le sous-arbre droit */
		(* a)->sad->nbRef += 1;

		/* Augmente la racine de l'arbre */
		* a = creer_arbre(nb1 + nb2, sad(* a), * a, NULL);
		
		/* Met à jour le père du sad (pas de sag car copie par référence) */
		(* a)->sad->pere = *a;
	}

}

/**
 * Diminue l'ordre de l'arbre de 1
 * @param a Arbre de Fibonacci à modifier
 */
void diminuerFibo(t_arbre * a){
	int val = 0;

	if(!arbre_vide(* a)){

		//libererSa(&(* a)->sag); // A revoir pose soucis dans un cas particulier (augmentation puis diminution)

		/* Récupère la valeur de la racine */
		val_racine(* a, &val);

		/* La racine est maintenant (fib n-1) en prenant des précautions sur fib(n) <= 2 */
		if(val >= 2)
			* a = sad(* a);
		else{
			/* Libère le dernier noeud */
			free(* a);
			* a = NULL;
		}

		/* Met à jour le père de la racine */
		if(* a && pere(* a)){
			free(pere(* a));
			(* a)->pere = NULL;
		}

	}
}

/* Programme principal */
int main(void)
{	
	int choix;	/* Choix de l'utilisateur */

	t_arbre a;
	
	/* Création de l'arbre de Fibonacci de base */	
	a = creer_arbre(5, NULL, NULL, NULL);

	ajout_gauche(a, 2);
	ajout_gauche(sag(a), 1);
	ajout_droit(sag(a), 1);

	ajout_droit(a, 3);
	ajout_gauche(sad(a), 1);
	ajout_droit(sad(a), 2);
	ajout_gauche(sad(sad(a)), 1);
	ajout_droit	(sad(sad(a)), 1);

	do
	{	/* Affichage du menu */
		printf("\nMenu :\n");
		printf(" 1 - Afficher l'arbre préfixe\n");
        printf(" 2 - Libérer l'arbre\n");
		printf(" 3 - Créer arbre de Fibonacci\n");
		printf(" 4 - Augmenter l'ordre\n");
		printf(" 5 - Diminuer l'ordre\n");
		printf(" 6 - Quitter\n");
		printf("Votre choix : ");
		scanf("%i",&choix);

		/* Traitement du choix de l'utilisateur */
		switch(choix)
		{	case 1: afficher_arbre(a, PREFIXE); break;
			case 2: liberer(&a); break;
			case 3: creer_abrFibo(&a); break;
			case 4: augmenterFibo(&a); break;
			case 5: diminuerFibo(&a); break;
			case 6: liberer(&a); break;
			default: printf("Erreur: votre choix doit être compris entre 1 et 6\n");
		}
	}
	while(choix!=6);
	printf("Au revoir !\n");
	
	return 0;
}


